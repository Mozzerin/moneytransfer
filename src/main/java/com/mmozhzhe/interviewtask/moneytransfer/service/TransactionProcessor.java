package com.mmozhzhe.interviewtask.moneytransfer.service;

import com.mmozhzhe.interviewtask.moneytransfer.rest.api.ApiException;
import com.mmozhzhe.interviewtask.moneytransfer.rest.model.Transaction;
import com.mmozhzhe.interviewtask.moneytransfer.rest.model.TransactionResponse;

public interface TransactionProcessor {

    TransactionResponse processTransaction(Transaction transaction) throws ApiException;
}
