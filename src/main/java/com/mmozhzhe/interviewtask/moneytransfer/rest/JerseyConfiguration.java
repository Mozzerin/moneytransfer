package com.mmozhzhe.interviewtask.moneytransfer.rest;

import org.glassfish.jersey.server.ResourceConfig;

public class JerseyConfiguration extends ResourceConfig {

    public JerseyConfiguration(){
        packages("com.mmozhzhe.interviewtask.moneytransfer.rest");
    }
}
