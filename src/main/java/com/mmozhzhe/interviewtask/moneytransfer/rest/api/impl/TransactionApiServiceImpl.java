package com.mmozhzhe.interviewtask.moneytransfer.rest.api.impl;

import com.mmozhzhe.interviewtask.moneytransfer.rest.api.ApiException;
import com.mmozhzhe.interviewtask.moneytransfer.rest.api.ApiResponseMessage;
import com.mmozhzhe.interviewtask.moneytransfer.rest.api.NotFoundException;
import com.mmozhzhe.interviewtask.moneytransfer.rest.api.TransactionApiService;
import com.mmozhzhe.interviewtask.moneytransfer.rest.model.Error;
import com.mmozhzhe.interviewtask.moneytransfer.rest.model.Transaction;
import com.mmozhzhe.interviewtask.moneytransfer.rest.model.TransactionResponse;
import com.mmozhzhe.interviewtask.moneytransfer.service.TransactionProcessor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class TransactionApiServiceImpl extends TransactionApiService {

    Logger logger = LogManager.getLogger(TransactionApiServiceImpl.class);

    private final TransactionProcessor transactionProcessor;

    public TransactionApiServiceImpl(TransactionProcessor transactionProcessor) {
        this.transactionProcessor = transactionProcessor;
    }

    @Override
    public Response handleTransaction(Transaction transaction, SecurityContext securityContext) throws NotFoundException {
        try {
            final TransactionResponse transactionResponse = transactionProcessor.processTransaction(transaction);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
        } catch (ApiException e) {
            logger.error(e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new Error()).build();
        }
    }
}
