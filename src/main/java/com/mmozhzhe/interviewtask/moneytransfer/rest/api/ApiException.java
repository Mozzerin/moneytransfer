package com.mmozhzhe.interviewtask.moneytransfer.rest.api;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen", date = "2018-10-26T23:32:38.551+03:00[Europe/Moscow]")
public class ApiException extends Exception{
    private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
