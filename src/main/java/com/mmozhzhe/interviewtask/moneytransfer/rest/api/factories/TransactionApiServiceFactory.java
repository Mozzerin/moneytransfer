package com.mmozhzhe.interviewtask.moneytransfer.rest.api.factories;

import com.mmozhzhe.interviewtask.moneytransfer.rest.api.TransactionApiService;
import com.mmozhzhe.interviewtask.moneytransfer.rest.api.impl.TransactionApiServiceImpl;
import com.mmozhzhe.interviewtask.moneytransfer.service.TransactionProcessorImpl;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen", date = "2018-10-26T23:39:10.107+03:00[Europe/Moscow]")
public class TransactionApiServiceFactory {
    private final static TransactionApiService service = new TransactionApiServiceImpl(new TransactionProcessorImpl());

    public static TransactionApiService getTransactionApi() {
        return service;
    }
}
